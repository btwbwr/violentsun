// ==========================================================================
// Copyright (C)2013 by Aaron Suen <warr1024@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// ---------------------------------------------------------------------------

package net.minecraft.src;

import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Type;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import net.minecraft.server.MinecraftServer;

public class SolEngineCore
	{
	public static SolEngineCore instance_ = null;
	public static SolEngineCore getInstance()
		{
		if(instance_ == null)
			instance_ = new SolEngineCore();
		return instance_;
		}

	public String chatColor(String colorCode)
		{
		return "\u00a7" + colorCode;
		}
	public void announce(NetServerHandler net, String message)
		{
		net.sendPacket(new Packet3Chat(chatColor("c") + "VSM: " + message));
		}
	public void serverPlayerConnectionInitialized(NetServerHandler net, EntityPlayerMP player)
		{
		announce(net, "Violent Sun BWR Mix-In DEVELOPMENT VERSION");
		}

	public void blockUpdateTick(World world, int x, int y, int z)
		{
		}

	public void entityLivingUpdate(EntityLiving self)
		{
		World world = self.worldObj;

		int x = MathHelper.floor_double(self.posX);
		int y = MathHelper.floor_double(self.posY + 1);
		int z = MathHelper.floor_double(self.posZ);
		int sun = world.getSavedLightValue(EnumSkyBlock.Sky, x, y, z)
			- world.skylightSubtracted - 8;

		if(sun > self.rand.nextInt(32))
			self.setFire(1);

		if((world.skylightSubtracted <= 0)
			&& (world.rand.nextInt(50) == 0)
			&& (self instanceof EntityPlayerMP))
			{
			x += self.rand.nextInt(224) - 112;
			z += self.rand.nextInt(224) - 112;
			y = world.getHeightValue(x, z) - 1;
			int id = world.getBlockId(x, y, z);
			if((id != 0) && !Block.blocksList[id].blockMaterial.isReplaceable())
				y++;
			world.setBlockAndMetadataWithNotify(x, y, z, Block.fire.blockID, world.rand.nextInt(16));
			}
		}

	public void playerSpawned(EntityPlayerMP player)
		{
		player.addPotionEffect(new PotionEffect(Potion.fireResistance.id, 1200, 0));
		}
	}
